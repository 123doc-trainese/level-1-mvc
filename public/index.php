<?php

/**
 * Composer
 */
require dirname(__DIR__) . '/vendor/autoload.php';


/**
 * Load Env
 */
$dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();

/**
 * Error and Exception handling
 */
error_reporting(E_ALL);
set_error_handler('\App\Core\Error::errorHandler');
set_exception_handler('\App\Core\Error::exceptionHandler');

/**
 * Routing
 */
$router = new \App\Core\Router();

// Add the routes
$router->add('/', [
    'controller' => 'HomeController',
    'action' => 'index'
]);

$router->dispatch($_SERVER['REQUEST_URI']);